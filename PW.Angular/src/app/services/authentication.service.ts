import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { UserCredentialModel, AppUser } from '../models';
import { environment } from '../../environments/environment';
import { Observer } from 'rxjs/Observer';
import { AuthUtility } from '../utility';
import { UserRegisterModel } from '../models/user-register.model';
import { BaseService } from '.';
import { isArray } from 'util';

@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { 
     }
    public login(credentials: UserCredentialModel): Observable<AppUser> {
        return this.token(credentials).flatMap(data => {
            if (data.access_token) {
                let _headers = new HttpHeaders({
                    'Authorization': `Bearer ${data.access_token}`,
                    'Content-Type': 'application/json; charset=utf-8'
                });
                return this.http.get<any>(environment.endpointApi + 'account', { headers: _headers })
                    .map(
                        (res: any) => {
                            const user = new AppUser(data.access_token, res.result.balance, res.result.email, res.result.userName);
                            AuthUtility.setCurrentUser(user);
                            return user;
                        },
                        err => {
                            if (err.status === 401 || err.status === 403) {
                                AuthUtility.logout();
                            }
                        }
                    )
            }
        });
    }

    private token(credentials: UserCredentialModel): Observable<any> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });

        const body = new HttpParams()
            .set('grant_type', 'password')
            .set('username', credentials.userName)
            .set('password', credentials.password);
        return this.http.post<any>(environment.endpointApi + 'connect/token', body.toString(), { headers: headers })
            .map(data =>
                data
            )
    }

    public register(newUser: UserRegisterModel): Observable<any> {
        const headers = new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' });
        return this.http.post<any>(environment.endpointApi + 'account/register', JSON.stringify(newUser), { headers: headers })
            .map(data => {
                if (data) {

                    return data;
                }
            });
    }

    public getRecipients(): Observable<AppUser[]> {
        let _headers = new HttpHeaders({
            'Authorization': `Bearer ${AuthUtility.CurrentUser.access_token}`,
            'Content-Type': 'application/json; charset=utf-8'
        });
        return this.http.get<any>(environment.endpointApi + 'account/users', { headers: _headers })
            .map(res => {
                if (res && res.result && isArray(res.result)) {
                    let result: AppUser[] = [];
                    for (let index = 0; index < res.result.length; index++) {
                        result.push(new AppUser("", res.result[index].balance, res.result[index].email, res.result[index].userName));
                    }
                    return result
                }
            }).catch(err => {
                if (err.status === 401 || err.status === 403) {
                    AuthUtility.logout();
                    return Observable.throw('Unauthorized');
                }
            });
    }

    public updateUserInfo(): Observable<AppUser> {
        let _headers = new HttpHeaders({
            'Authorization': `Bearer ${AuthUtility.CurrentUser.access_token}`,
            'Content-Type': 'application/json; charset=utf-8'
        });
        return this.http.get<any>(environment.endpointApi + 'account', { headers: _headers })
            .map(
                (res: any) => {
                    if (res && res.result) {
                        let user = AuthUtility.CurrentUser;
                        user.Balance = res.result.balance;
                        AuthUtility.setCurrentUser(user);
                        return user;
                    }
                    return null;
                }).catch(err => {
                    if (err.status === 401 || err.status === 403) {
                        AuthUtility.logout();
                        return Observable.throw('Unauthorized');
                    }
                });
    }
}