import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaseService } from './base.service';
import { TransactionModel, AppUser } from '../models';
import { isArray } from 'util';
import { TransactionsFilterModel } from '../models/transactions-filter.model';
import { AuthUtility } from '../utility';

@Injectable()
export class TransactionService extends BaseService {

    constructor(private http: HttpClient) {
        super();
    }

    public createTransaction(name: string, amount: number): void {
        var requestData = {
            Amount: amount,
            Recipient: name
        };
        this.http.post(this.endpointApi + 'operation/create', JSON.stringify(requestData), { headers: this.headers })
            .subscribe(data => {
            },
                err => {
                    if (err.status === 401 || err.status === 403) {
                        AuthUtility.logout();
                    }
                }
            );
    }

    public findTransactions(filter: TransactionsFilterModel): Observable<TransactionModel[]> {
        return this.http.post<any>(this.endpointApi + 'operation', JSON.stringify(filter), { headers: this.headers })
            .map(res => {
                if (res && res.result && isArray(res.result.items)) {
                    const result: TransactionModel[] = res.result.items;
                    result[0].paging = res.result.paging;
                    return result;
                }
            }
            ).catch(err => {
                if (err.status === 401 || err.status === 403) {
                    AuthUtility.logout();
                    return Observable.throw('Unauthorized');
                }
            });
    }
}