import { HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { AuthUtility } from '../utility';

export class BaseService {

    private _headers: HttpHeaders;

    constructor() {
    }

    protected get headers(): HttpHeaders {
        this._headers = new HttpHeaders({
            'Authorization': `Bearer ${AuthUtility.getToken()}`,
            'Content-Type': 'application/json; charset=utf-8'
        });

        return this._headers;
    }

    protected get endpointApi(): string {
        return environment.endpointApi
    }
}