import { AppUser } from '../models';

export class AuthUtility {
    public static readonly CURRENT_USER: string = 'c_user';

    public static get CurrentUser(): AppUser {
        if (localStorage.getItem(this.CURRENT_USER)) {
            return JSON.parse(localStorage.getItem(this.CURRENT_USER));
        }
    }

    public static setCurrentUser(user: AppUser) {
        localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
    }

    public static get IsAuthenticated(): boolean {
        if (localStorage.getItem(this.CURRENT_USER)) {
            return true;
        }
        return false;
    }

    public static logout() {
        localStorage.removeItem(this.CURRENT_USER);
    }

    public static getToken(): string {
        if (this.IsAuthenticated) {
            return this.CurrentUser.access_token;
        }
        return null;
    }
}