import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';

import {
   LoginComponent
} from './components/login/login.component';
import {
  AppComponent
} from './components/app/app.component'
import { MaterialModule } from './material.module';
import { EqualValidator } from './directives/equal-validator.directive';
import { TransactionsComponent, RepeatTransactionDialog } from './components/transactions/transactions.component';
import { CreateTransactionComponent, CreateTransactionDialog } from './components/create-transaction/create-transaction.component';
import { TransactionService, AuthenticationService } from './services';



@NgModule({
  declarations: [
    AppComponent,    
    LoginComponent, EqualValidator, TransactionsComponent, CreateTransactionComponent, CreateTransactionDialog, RepeatTransactionDialog
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,  
    ReactiveFormsModule, 
  ],
  entryComponents: [CreateTransactionDialog, RepeatTransactionDialog],
  exports: [
    MaterialModule
  ],
  providers: [
    TransactionService, AuthenticationService
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }