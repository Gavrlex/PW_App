import { TransactionModel } from '../models';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { TransactionService } from '../services';
import { CollectionViewer, DataSource } from "@angular/cdk/collections";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TransactionsFilterModel } from '../models/transactions-filter.model';
import { catchError, switchMap, finalize } from "rxjs/operators";
import { Subscription } from 'rxjs/Subscription';

export class TransactionsDataSource implements DataSource<TransactionModel> {

    private transactionsSubject = new BehaviorSubject<TransactionModel[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);
    public countItems: number = 0;

    public loading$ = this.loadingSubject.asObservable();

    constructor(private transactionsService: TransactionService) { }

    connect(collectionViewer: CollectionViewer): Observable<TransactionModel[]> {
        console.log("Connecting data source");
        return this.transactionsSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.transactionsSubject.complete();
        this.loadingSubject.complete();
    }


    loadTransactions(pageIndex = 1, pageSize = 5, filter = null, amountFrom = null, amountTo = null, startDateTime = null, endDateTime = null,
        sortAmount = "", sortDate = "", sortName = "") {

        this.loadingSubject.next(true);
        const filterModel: TransactionsFilterModel = new TransactionsFilterModel();
        filterModel.pageNumber = pageIndex;
        filterModel.pageSize = pageSize;
        filterModel.amountFrom = amountFrom;
        filterModel.amountTo = amountTo;
        filterModel.correspondentName = filter;
        filterModel.startDateTime = startDateTime;
        filterModel.endDateTime = endDateTime;
        filterModel.sortAmount = sortAmount;
        filterModel.sortDate = sortDate;
        filterModel.sortName = sortName;
        this.transactionsService.findTransactions(filterModel).pipe(
            catchError(() => Observable.of([])),
            finalize(() => this.loadingSubject.next(false))
        ).subscribe(data => {
            this.transactionsSubject.next(data)
            if (data[0])
                this.countItems = data[0].paging.totalItems;
        }
        );
    }
}
