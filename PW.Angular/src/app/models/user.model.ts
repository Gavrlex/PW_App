export class AppUser {
    constructor(public access_token: string,
        public Balance: number,
        public Email: string,
        public UserName: string) { }
}