export class PagingModel {

    public pageNumber: number = 1;
    public pageSize: number = 10;

    constructor() { }
}