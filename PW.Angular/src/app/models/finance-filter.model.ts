export class FinanceFilterModel {
    constructor(public resourceType: number = 0,
        public year: number = null,
        public month: number = null,) { }

    public setDefaultDate() {
        this.year = new Date().getFullYear();
        this.month = new Date().getMonth() - 1;
    }
}
