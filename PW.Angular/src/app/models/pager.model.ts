export class Pager {
    public totalItems: number = 0;
    public pageNumber: number = 0;
    public pageSize: number = 0;
    public totalPages: number = 0;

    constructor() { }
}