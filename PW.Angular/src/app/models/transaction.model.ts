import { AppUser, Pager } from 'app/models';

export class TransactionModel {
    public id: string = '';
    public operationDateUTC: Date= null;
    public senderUserID: string = '';
    public recipientUserID: string = '';
    public amount: number = null;
    public senderResultingBalance: number = null;
    public recipientResultingBalance: number = null;
    public senderUser: AppUser = null;
    public recipientUser: AppUser = null;
    public paging: Pager = null;
}