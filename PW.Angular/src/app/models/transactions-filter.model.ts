import { PagingModel } from './paging.model';

export class TransactionsFilterModel extends PagingModel {
    
    public startDateTime: Date = null;
    public endDateTime: Date = null;
    public amountFrom: number = null;
    public amountTo: number = null;
    public correspondentName: string = null;
    public sortDate: string = null;
    public sortName: string = null;
    public sortAmount: string = null;

    constructor() {
        super();
    }
}