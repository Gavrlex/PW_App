import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef, Inject } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort, MatDatepicker, MatTable, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TransactionsDataSource } from '../../classes/transactions-data-source';
import { TransactionService } from '../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { debounceTime, distinctUntilChanged, startWith, tap, delay } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';

@Component({
  selector: 'transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css'],
  providers: [TransactionService]
})
export class TransactionsComponent implements OnInit, AfterViewInit {

  dataSource: TransactionsDataSource;
  displayedColumns = ['operationDateUTC', 'correspondent', 'amount',
    'resultBalance', 'buttons'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('inputCor') input: ElementRef;
  @ViewChild('pickerFrom') pickerFrom: MatDatepicker<Date>;
  @ViewChild('pickerTo') pickerTo: MatDatepicker<Date>;
  @ViewChild('amountFrom') amountFrom: ElementRef;
  @ViewChild('amountTo') amountTo: ElementRef;

  constructor(private transactionService: TransactionService, private route: ActivatedRoute, private router: Router, public dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource = new TransactionsDataSource(this.transactionService);
    this.dataSource.loadTransactions(1, this.paginator.pageSize, null, null, null, null, null, null, 'asc', null);
  }
  ngAfterViewInit() {
    // server-side search
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadTransactionsPage();
        })
      )
      .subscribe();

    fromEvent(this.amountFrom.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadTransactionsPage();
        })
      )
      .subscribe();

    fromEvent(this.amountTo.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadTransactionsPage();
        })
      )
      .subscribe();
    merge(this.pickerFrom._datepickerInput.dateChange, this.pickerFrom._datepickerInput.dateInput)
      .pipe(debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadTransactionsPage();
        })
      )
      .subscribe(date => console.log(date));

    merge(this.pickerTo._datepickerInput.dateChange, this.pickerTo._datepickerInput.dateInput)
      .pipe(debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadTransactionsPage();
        })
      )
      .subscribe(date => console.log(date));

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    // // on sort or paginate events, load a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        tap(() => this.loadTransactionsPage())
      )
      .subscribe();
  }

  loadTransactionsPage(refresh = false) {
    this.dataSource.loadTransactions(
      this.paginator.pageIndex + 1,
      this.paginator.pageSize,
      this.input.nativeElement.value,
      this.amountFrom.nativeElement.value, this.amountTo.nativeElement.value,
      this.pickerFrom.startAt ? new Date(this.pickerFrom.startAt.toLocaleString()).toLocaleString() : null,
      this.pickerTo.startAt ? new Date(this.pickerTo.startAt.toLocaleString()).toLocaleString() : null,
      this.sort.active === "amount" ? this.sort.direction : null,
      this.sort.active === "operationDateUTC" ? this.sort.direction : null, this.sort.active === "correspondent" ? this.sort.direction : null
    );
  }
  public refresh() {
    this.paginator.pageIndex = 0;
    setTimeout(() => {
      this.loadTransactionsPage();
    }, 200);
  }

  public onRetry(user, amount) {
    let dialogRef = this.dialog.open(RepeatTransactionDialog, {
      width: '250px',
      data: { user: user, amount: amount }
    });
    dialogRef.afterClosed().subscribe(result => {
      this.refresh();
    });
  }
}

@Component({
  selector: 'repeat-transaction-dialog',
  templateUrl: 'dialog-repeat-transaction.html',
})
export class RepeatTransactionDialog {

  constructor(
    public dialogRef: MatDialogRef<RepeatTransactionDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private transactionService: TransactionService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOkClick(): void {
    if (this.data.user && this.data.amount)
      this.transactionService.createTransaction(this.data.user, this.data.amount);
    this.dialogRef.close();
  }
}