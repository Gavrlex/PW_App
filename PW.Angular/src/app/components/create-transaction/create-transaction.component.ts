import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MatDialog, ErrorStateMatcher } from '@angular/material';
import { FormControl, FormGroupDirective, NgForm, Validators, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { Subscription } from 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/observable/of';
import "rxjs/add/operator/switchMap";
import { AuthUtility } from '../../utility';
import { AppUser } from '../../models';
import { isUndefined } from 'util';
import { TransactionService, AuthenticationService } from '../../services';

@Component({
  selector: 'create-transaction',
  templateUrl: './create-transaction.component.html',
  styleUrls: ['./create-transaction.component.css']
})
export class CreateTransactionComponent {

  @Output() onCreate = new EventEmitter();

  constructor(public dialog: MatDialog) { }

  openDialog() {
    const dialogRef = this.dialog.open(CreateTransactionDialog, {
      height: '350px'
    });
    const sub = dialogRef.componentInstance.onCreate.subscribe((data) => {
      this.onCreate.emit();
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      sub.unsubscribe();
    });
  }
}
export class User {
  constructor(public name: string) { }
}
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'dialog-create-transaction',
  templateUrl: 'dialog-create-transaction.html',
  styleUrls: ['./create-transaction.component.css']
})
export class CreateTransactionDialog {
  constructor(private transactionService: TransactionService, private authenticationService: AuthenticationService) { }
  public amountControl = new FormControl('', [
    Validators.required,
    Validators.min(0.0000001),
    Validators.max(AuthUtility.CurrentUser.Balance)
  ]);

  public myControl = new FormControl('');
  flagForm: FormGroup = new FormGroup({ 'myControl': this.myControl, 'amountControl': this.amountControl });
  matcher = new MyErrorStateMatcher();
  options: Observable<AppUser[]>;
  filteredOptions: Observable<AppUser[]>;
  onCreate = new EventEmitter();

  ngOnInit() {
    this.options = this.authenticationService
      .getRecipients();
    this.filteredOptions = this.myControl.valueChanges
      .startWith(null)
      .switchMap(val => {
        if (val) {
          return this.filter(val)
        } else {
          return this.filter('something else')
        }
      })
  }

  filter(val: string): Observable<AppUser[]> {
    return this.options
      .map(response => response.filter(option => {
        return option.UserName.toLowerCase().indexOf(val.toLowerCase()) === 0
      }));
  }

  changeMyControl(name: string): void {
    setTimeout(() => {
      this.options.toPromise().then(res => {
        if (res.filter(bt => bt.UserName == name).length < 1) {
          this.myControl.setErrors({ 'incorrect': true });
        }
      });
    }, 200);
  }

  createTransaction(recipient: string, amount: number): void {
    this.transactionService.createTransaction(recipient, amount);
    this.onCreate.emit();
  }
}

