import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { AuthenticationService } from '../../services';
import { AuthUtility } from '../../utility';
import { UserCredentialModel } from '../../models';
import { UserRegisterModel } from '../../models/user-register.model';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['login.component.css'],
  providers: [AuthenticationService]
})
export class LoginComponent implements OnInit {
  hide = true;
  hidePass = true;
  hideConfirmPass = true;
  public showLogin: boolean = true;
  public loading = false;
  public model: UserCredentialModel = new UserCredentialModel();
  private modelAfterReg: UserCredentialModel  = new UserCredentialModel();
  public regModel: UserRegisterModel = new UserRegisterModel();
  public returnUrl: string = document.getElementsByTagName('base')[0].href;

  public hasErrors: boolean = false;
  public error: string = '';
  public errors: Array<string>;

  constructor(private route: ActivatedRoute, private titleService: Title, private router: Router,
    private authService: AuthenticationService) { }

  ngOnInit() {
    if (AuthUtility.IsAuthenticated) {
      this.router.navigate(['/']);
      return;
    }

    this.titleService.setTitle('Authorization');

    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.loading = true;
    this.error = '';
    this.hasErrors = false;
    this.authService.login(this.model)
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        resp => {
          this.hasErrors = true;
          this.error = resp.error.error_description;
          this.loading = false;
        });
  }
  register() {
    this.loading = true;
    this.error = '';
    this.hasErrors = false;
    this.authService.register(this.regModel)
      .subscribe(
        data => {
          this.modelAfterReg.userName = this.regModel.Email;
          this.modelAfterReg.password = this.regModel.Password;
          this.authService.login(this.modelAfterReg)
            .subscribe(
              data => {
                this.router.navigate([this.returnUrl]);
              },
              resp => {
                this.hasErrors = true;
                this.error = resp.error.error_description;
                this.loading = false;
              });
        },
        resp => {
          this.hasErrors = true;
          this.errors = resp.error;
          this.loading = false;
        });
  }
  changeShow() {
    this.showLogin = !this.showLogin;
  }
}
