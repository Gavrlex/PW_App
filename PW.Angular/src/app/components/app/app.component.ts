import { Component, ViewChild, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import {
  Title, DomSanitizer,
  SafeHtml,
  SafeUrl,
  SafeStyle
} from '@angular/platform-browser';

import { AuthUtility } from '../../utility';
import { AppUser } from '../../models';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TransactionsComponent } from '../transactions/transactions.component';
import { AuthenticationService } from '../../services/authentication.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [],
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy {
  public logo: string = require('../../assets/img/logo.png');

  apiTimer: any;

  @ViewChild(TransactionsComponent) transactions: TransactionsComponent;

  constructor(private router: Router, private titleService: Title,
    public dialog: MatDialog, private sanitization: DomSanitizer, private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    this.titleService.setTitle('PW');
    // Здесь вместо таймера на обновления баланса юзера можно использовать веб сокеты
    this.apiTimer = setInterval(() => {
      console.log("update balance");
      this.authenticationService.updateUserInfo().subscribe(res => { });
    }, (5 * 1000));
  }

  ngOnDestroy() {
    if (this.apiTimer) {
      clearInterval(this.apiTimer);
      this.apiTimer = -1;
    }
  }

  public get CurrentUser(): AppUser {
    return AuthUtility.CurrentUser;
  }

  public get IsAuthenticated(): boolean {
    let res = AuthUtility.IsAuthenticated;

    if (!res) {
      clearInterval(this.apiTimer);
      this.apiTimer = -1;
    }
    else 
    {
      if (this.apiTimer==-1)
      {
        this.apiTimer = setInterval(() => {
          console.log("update balance");
          this.authenticationService.updateUserInfo().subscribe(res => { });
        }, (5 * 1000));
      }
    }
    return res;
  }

  public logout() {
    AuthUtility.logout();
    clearInterval(this.apiTimer);
    this.apiTimer = -1;
    //this.router.navigate(['/login']);
  }

  updateHistory() {
    this.authenticationService.updateUserInfo().subscribe(res => { });
    this.transactions.refresh();
  }
}