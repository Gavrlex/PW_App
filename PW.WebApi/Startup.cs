﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using PW.DAL;
using PW.DAL.Contracts;
using PW.Data.Entities;
using Microsoft.AspNetCore.Identity;
using PW.DTO.Mappings;
using PW.BLL.Mappings;
using AutoMapper;
using PW.BLL.Contracts;
using PW.BLL.Services;
using Microsoft.AspNetCore.Http;
using AspNet.Security.OpenIdConnect.Primitives;
using PW.Web.Extensions;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using PW.WebApi.Extensions;

namespace PW.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            ConfigureDependencyInjection(services);
            ConfigureAuth(services);

            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IUrlHelper>(factory =>
            {
                var actionContext = factory.GetService<IActionContextAccessor>()
                                           .ActionContext;
                return new UrlHelper(actionContext);
            });

            services.AddEntityFrameworkSqlServer()
                .AddDbContext<PWDbContext>(options =>
                {
                    options.UseSqlServer(Configuration.GetConnectionString("Default"));
                    options.UseOpenIddict();
                });
            services.AddScoped<IPWDbContext>(provider => provider.GetService<PWDbContext>());
            services.AddCors();
            services.AddMvc();
            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseCors(builder => builder.WithOrigins("http://localhost:4200").AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            app.UseMvc();
            // убрал условие, чтобы база развернулась при первом запуске
            //if (!env.IsDevelopment())
            //{
                app.ApplyMigrations();
            //}
        }

        private void ConfigureDependencyInjection(IServiceCollection services)
        {
            // mapper
            services.AddAutoMapper(typeof(DtoProfile), typeof(BllProfile));

            // services
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IGenericRepositoryFactory, GenericRepositoryFactory>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ISecurityContext>(provider =>
            {
                var http = provider.GetService<IHttpContextAccessor>();
                return new SecurityContext(() => http.HttpContext.User.Identity.GetId());
            });
            services.AddSingleton<ITransactionService, TransactionService>();
            services.AddSingleton<IMigrator, Migrator>();
        }

        private void ConfigureAuth(IServiceCollection services)
        {
            // identity
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<PWDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.ClaimsIdentity.UserNameClaimType = OpenIdConnectConstants.Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = OpenIdConnectConstants.Claims.Role;
            });

            services.AddOpenIddict(options =>
            {
                options.AddEntityFrameworkCoreStores<PWDbContext>();
                options.AddMvcBinders();
                options.EnableTokenEndpoint("/connect/token");
                options
                    .AllowPasswordFlow();
                // During development, you can disable the HTTPS requirement.
                options.DisableHttpsRequirement();
            });

            services.AddAuthentication()
                .AddOAuthValidation();
        }
    }
}
