﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using AspNet.Security.OAuth.Validation;
using PW.BLL.Contracts;
using PW.DTO.DTO;
using PW.Data.Models;
using PW.Web.Extensions;
using PW.DTO.Paging;
using PW.Data.Entities;

namespace PW.WebApi.Controllers
{
    [Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
    [Route("operation")]
    public class OperationController : BaseController
    {
        private readonly ITransactionService _transactionService;
        private readonly IUrlHelper _urlHelper;

        public OperationController(ITransactionService transactionService, AutoMapper.IMapper mapper, IUrlHelper urlHelper) : base(mapper)
        {
            _transactionService = transactionService;
            _urlHelper = urlHelper;
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]OperationParamsDTO dto)
        {
            var result = await ProcessBllRequest(async () =>
            {
                _transactionService.Create(Mapper.Map<OperationParamsModel>(dto));
            });
            return DeafaultResponse(result);
        }

        [HttpPost]
        public async Task<IActionResult> History([FromBody]TransactionsPagingParams pagingParams)
        {
            var result = await ProcessBllRequest<TransactionHistoryDTO>(async () =>
            {
                    var model = await _transactionService.GetTransactions(Mapper.Map<TransactionsPagingParams>(pagingParams));
                    Response.Headers.Add("X-Pagination", model.GetHeader().ToJson());
                    var outputModel = new TransactionHistoryDTO
                    {
                        Paging = model.GetHeader(),
                        Links = GetLinks(model),
                        Items = model.List.Select(m => Mapper.Map<TransactionDTO>(Mapper.Map<TransactionModel>(m))).ToList(),
                    };
                    return outputModel;
            });
            return DeafaultResponse(result);

        }

        #region private
        private List<LinkInfo> GetLinks(PagedList<Transaction> list)
        {
            var links = new List<LinkInfo>();

            if (list.HasPreviousPage)
                links.Add(CreateLink("GetMovies", list.PreviousPageNumber,
                           list.PageSize, "previousPage", "GET"));

            links.Add(CreateLink("GetMovies", list.PageNumber,
                           list.PageSize, "self", "GET"));

            if (list.HasNextPage)
                links.Add(CreateLink("GetMovies", list.NextPageNumber,
                           list.PageSize, "nextPage", "GET"));

            return links;
        }

        private LinkInfo CreateLink(
            string routeName, int pageNumber, int pageSize,
            string rel, string method)
        {
            return new LinkInfo
            {
                Href = _urlHelper.Link(routeName,
                            new { PageNumber = pageNumber, PageSize = pageSize }),
                Rel = rel,
                Method = method
            };
        }
        #endregion
    }
}