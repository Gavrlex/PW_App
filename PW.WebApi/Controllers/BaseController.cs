﻿using System;
using System.Threading.Tasks;
using PW.Web.DTO;
using PW.Web.Extensions;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PW.WebApi.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IMapper Mapper;
        public BaseController(IMapper mapper)
        {
            Mapper = mapper;
        }

        protected async Task<object> ProcessBllRequest(Func<Task> bllActionTask)
        {
            try
            {
                await bllActionTask();
                return CommonResponseExtensions<string>.Success();
            }
            catch (ArgumentException e)
            {
                var error = CommonResponseExtensions<string>.Exception(StatusCodes.Status409Conflict, e);
                return error;
            }
        }

        protected async Task<object> ProcessBllRequest<T>(Func<Task<T>> bllActionTask) where T : class 
        {
            try
            {
                var result = await bllActionTask();
                return CommonResponseExtensions<T>.Success(result);
            }
            catch (ArgumentException e)
            {
                var error = CommonResponseExtensions<string>.Exception(StatusCodes.Status409Conflict, e);
                return error;
            }
        }

        protected IActionResult DeafaultResponse(object data)
        {
            return Json(data);
        }
    }
}