﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PW.WebApi.Controllers
{
    public class HomeController : Controller
    {
        [ResponseCache(NoStore = true)]
        public IActionResult Index()
        {
            return File("index.html", "text/html"); ;
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}