﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using PW.Data.Entities;
using PW.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using PW.DTO.DTO;
using AspNet.Security.OAuth.Validation;
using PW.Data.Models;
using PW.BLL.Contracts;
using PW.Web.DTO;
using Microsoft.AspNetCore.Http;
using System.Linq;

namespace PW.WebApi.Controllers
{
    [Authorize(AuthenticationSchemes = OAuthValidationDefaults.AuthenticationScheme)]
    [Route("account")]
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService, AutoMapper.IMapper mapper) : base(mapper)
        {
            _accountService = accountService;
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegisterDTO dto)
        {
            var result =
                !ModelState.IsValid
                    ? CommonResponseExtensions<string>.ValidationFailed(ModelState)
                    : await ProcessBllRequest(() => _accountService.Register(Mapper.Map<RegisterModel>(dto)));
            if (!(result is CommonResponse<string>) || (result as CommonResponse<string>).Code==StatusCodes.Status200OK)
            {
                return DeafaultResponse(result);
            }
            else
            {
                var err = result as CommonResponse<string>;
                return StatusCode(err.Code, err.Result);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            var result = await ProcessBllRequest(async () =>
            {
                var data = await _accountService.GetById(User.Identity.GetId().ToString());
                return Mapper.Map<UserDTO>(data);
            });
            return DeafaultResponse(result);
        }
        [HttpGet("balance")]
        public async Task<IActionResult> GetBalance()
        {
            var result = await ProcessBllRequest(async () =>
            {
                var data = await _accountService.GetById(User.Identity.GetId().ToString());
                return Mapper.Map<UserDTO>(data);
            });
            return DeafaultResponse(result);
        }

        [HttpGet("users")]
        public async Task<IActionResult> GetUsers()
        {
            var result = await ProcessBllRequest(async () =>
            {
                var data = await _accountService.GetOtherUsers(User.Identity.GetId().ToString());
                return data.Select(x=> Mapper.Map<UserDTO>(x)).ToList();
            });
            return DeafaultResponse(result);
        }

        #region private
        private async Task<ClaimsIdentity> GetIdentity(string email, string password)
        {
            ApplicationUser user = await _accountService.AuthorizeUser(email, password);
            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                    new Claim(ClaimTypes.NameIdentifier, user.Id)
                };
                ClaimsIdentity claimsIdentity =
                new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
            // если пользователя не найдено
            return null;
        }
        #endregion
    }
}
