﻿using System;
using System.Linq;
using PW.Web.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace PW.Web.Extensions
{
    public static class CommonResponseExtensions<T> where T : class
    {
        public static CommonResponse<T> Success(T res = null)
        {
            return
                new CommonResponse<T>
                {
                    Code = StatusCodes.Status200OK,
                    Result = res
                };
        }

        public static CommonResponse<string[]> ValidationFailed(ModelStateDictionary modelState)
        {
            return
                new CommonResponse<string[]>
                {
                    Code = StatusCodes.Status400BadRequest,
                    Result = modelState
                        .Select(x => x.Key)
                        .SelectMany(x => modelState[x].Errors.Select(e => e.ErrorMessage))
                        .ToArray()
                };
        }

        public static CommonResponse<string> Exception(int code, Exception e)
        {
            return
                new CommonResponse<string>
                {
                    Code = code,
                    Result = e.Message
                };
        }
    }
}
