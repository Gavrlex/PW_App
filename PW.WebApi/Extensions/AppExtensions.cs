﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using PW.Data.Entities;
using PW.DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
//using OpenIddict.Core;
//using OpenIddict.Models;

namespace PW.WebApi.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static void ApplyMigrations(this IApplicationBuilder app)
        {
            try
            {
                using (var serviceScope = app.ApplicationServices.CreateScope())
                {
                    new Migrator(
                            serviceScope.ServiceProvider.GetService<PWDbContext>())
                            //serviceScope.ServiceProvider.GetService<IdentityDbContext>())
                        .Migrate();
                }
            }
            catch (AggregateException e)
            {
                throw new Exception(e.ToString());
            }
        }
    }
}
