﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using AspNet.Security.OpenIdConnect.Extensions;
using AspNet.Security.OpenIdConnect.Primitives;

namespace PW.Web.Extensions
{
    public static class IdentityExtensions
    {
        public static Guid GetId(this IIdentity identity)
        {
            const string message = "Current user is not authenticated.";
            if (!identity.IsAuthenticated)
                throw new Exception(message);
            var idStr = (identity as ClaimsIdentity).GetClaim(OpenIdConnectConstants.Claims.Subject);
            var success = Guid.TryParse(idStr, out var result);
            if (!success)
                throw new Exception(message);
            return result;
        }
    }
}
