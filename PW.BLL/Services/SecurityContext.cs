﻿using System;
using PW.BLL.Contracts;

namespace PW.BLL.Services
{
    public class SecurityContext: ISecurityContext
    {
        private readonly Func<Guid> _currentUserIdGetter;

        public SecurityContext(Func<Guid> currentUserIdGetter)
        {
            _currentUserIdGetter = currentUserIdGetter;
        }

        public Guid CurrentUserId()
        {
            if(_currentUserIdGetter == null)
                throw new Exception("Security context has wtong configuration");
            return _currentUserIdGetter();
        }
    }
}
