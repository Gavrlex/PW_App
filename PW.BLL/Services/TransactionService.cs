﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using PW.BLL.Contracts;
using PW.Data.Entities;
using PW.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PW.BLL.Services;
using PW.DAL.Contracts;
using Newtonsoft.Json;
using System.Linq;
using System.Transactions;
using Microsoft.EntityFrameworkCore.Storage;
using System.IO;
using PW.DTO.Paging;

namespace PW.BLL.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccountService _accountService;
        private readonly ISecurityContext _securityContext;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ILogger<TransactionService> _logger;

        ArgumentException GetException(string s) => new ArgumentException(JsonConvert.SerializeObject(new[] { s }));
        ArgumentException GetException(IEnumerable<string> s) =>
            new ArgumentException(
                JsonConvert.SerializeObject(
                    s?.ToArray()));

        private static object s_SyncRoot = new object();

        private static class ExceptionMessages
        {
            public const string BalanceNotEnough = "Insufficient balance for money transfer.";
            public const string RecipientNotFound = "Recipient not found.";
            public const string TransferOneselfImpossble = "Translation to oneself is not possible";
            public const string AmountMustGreaterZero = "Amount must greater than 0.";
            public const string TransactionIdIsNotValid = "Transaction id is not valid.";
            public const string TransactionNotFound = "Transaction not found.";
            public const string PagingError = "Page number and page size must greater than 0.";
            public const string InternalServerError = "Internal Server Error";
        }

        public TransactionService(
           IUnitOfWork unitOfWork,
           IAccountService accountService,
           ISecurityContext securityContext,
           IMapper mapper,
           IConfiguration configuration,
           ILogger<TransactionService> logger)
        {
            _unitOfWork = unitOfWork;
            _accountService = accountService;
            _securityContext = securityContext;
            _mapper = mapper;
            _configuration = configuration;
            _logger = logger;
        }

        public void Create(OperationParamsModel model)
        {
            if (model.Amount <= 0)
            {
                throw GetException(ExceptionMessages.AmountMustGreaterZero);
            }
            // здесь блокировка используется для того чтобы ограничить одновременное проведение транзакций (возможно одновременное изменение счета)
            // по хорошему нужно сформировать регулятор проведение транзакций (например очередь транзакций и последовательное их выполнение)
            lock (s_SyncRoot)
            {
                var Sender = _accountService.GetById(_securityContext.CurrentUserId().ToString()).Result;
                var Recipient = _accountService.GetByName(model.Recipient).Result;
                if (Recipient == null)
                {
                    throw GetException(ExceptionMessages.RecipientNotFound);
                }
                if (Sender.Id.Equals(Recipient.Id))
                {
                    throw GetException(ExceptionMessages.TransferOneselfImpossble);
                }
                if (Sender.Balance < model.Amount)
                {
                    throw GetException(ExceptionMessages.BalanceNotEnough);
                }
                using (var t = _unitOfWork.BeginTransaction())
                    try
                    {
                        var newBalanceSender = Sender.Balance - model.Amount;
                        var newBalanceRecipient = Recipient.Balance + model.Amount;
                        var senderUpdate = _accountService.ChangeBalance(Sender.Id.ToString(), newBalanceSender).Result;
                        var recipientUpdate = _accountService.ChangeBalance(Recipient.Id.ToString(), newBalanceRecipient).Result;
                        _unitOfWork.TransactionRepository.Create(new Data.Entities.Transaction()
                        {
                            OperationDateUTC = DateTime.UtcNow,
                            Amount = model.Amount,
                            SenderUserID = Sender.Id.ToString(),
                            RecipientUserID = Recipient.Id.ToString(),
                            RecipientResultingBalance = newBalanceRecipient,
                            SenderResultingBalance = newBalanceSender
                        });
                        _unitOfWork.SaveChanges();
                        t.Commit();
                    }
                    catch (Exception e)
                    {
                        t.Rollback();
                        _logger.LogError(e, "Error occurred during save information about transaction.");
                        throw GetException(ExceptionMessages.InternalServerError);
                    }
            }
        }

        public async Task<PagedList<Data.Entities.Transaction>> GetTransactions(TransactionsPagingParams pagingParams)
        {
            if (pagingParams.PageNumber < 1 || pagingParams.PageSize < 1)
            {
                throw GetException(ExceptionMessages.PagingError);
            }
            try
            {
                var query = await _unitOfWork.TransactionRepository.GetAsync(
                    x => x.SenderUserID == _securityContext.CurrentUserId().ToString(),
                    send => send.SenderUser, rec => rec.RecipientUser
                    );
                if (pagingParams.StartDateTime.HasValue)
                {
                    query = query.Where(x =>  x.OperationDateUTC.Date >= pagingParams.StartDateTime.Value.Date);
                }
                if (pagingParams.EndDateTime.HasValue)
                {
                    query = query.Where(x => x.OperationDateUTC.Date <= pagingParams.EndDateTime.Value.Date);
                }
                if (pagingParams.AmountFrom.HasValue)
                {
                    query = query.Where(x => x.Amount >= pagingParams.AmountFrom.Value);
                }
                if (pagingParams.AmountTo.HasValue)
                {
                    query = query.Where(x => x.Amount <= pagingParams.AmountTo.Value);
                }
                if (!string.IsNullOrEmpty(pagingParams.CorrespondentName))
                {
                    query = query.Where(x => x.RecipientUser.UserName.ToLower().Contains(pagingParams.CorrespondentName.ToLower()));
                }
                if (!string.IsNullOrEmpty(pagingParams.SortDate))
                {
                    if (pagingParams.SortDate =="desc")
                    {
                        query = query.OrderByDescending(x => x.OperationDateUTC);
                    }
                    else
                    {
                        query = query.OrderBy(x => x.OperationDateUTC);
                    }
                }
                else
                {
                    query = query.OrderByDescending(x => x.OperationDateUTC);
                }
                if (!string.IsNullOrEmpty(pagingParams.SortName))
                {
                    if (pagingParams.SortName == "desc")
                    {
                        query = query.OrderByDescending(x => x.RecipientUser.UserName);
                    }
                    else
                    {
                        query = query.OrderBy(x => x.RecipientUser.UserName);
                    }
                }
                if (!string.IsNullOrEmpty(pagingParams.SortAmount))
                {
                    if (pagingParams.SortAmount == "desc")
                    {
                        query = query.OrderByDescending(x => x.Amount);
                    }
                    else
                    {
                        query = query.OrderBy(x => x.Amount);
                    }
                }
                return new PagedList<Data.Entities.Transaction>(
                    query, pagingParams.PageNumber, pagingParams.PageSize);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occurred during get information about transaction.");
                throw GetException(ExceptionMessages.InternalServerError);
            }
        }

    }
}
