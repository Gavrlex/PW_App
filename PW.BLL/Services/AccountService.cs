﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PW.BLL.Contracts;
using PW.Data.Entities;
using PW.Data.Models;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace PW.BLL.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ILogger<AccountService> _logger;
        private const decimal StartBalance = 500;

        private static class ExceptionMessages
        {
            public const string UserNotFound = "User not found.";
            public const string IncorrectPassword = "Incorrect password.";
            public const string EmailAlreadyExist = "Please, specify another email, this already exist.";
            public const string InternalServerError = "Internal Server Error";
            public const string EmailNotExistOrIncorrectPassword = "Email does not exist or incorrect password.";
            public const string EmailOrPasswordNotNull = "Email or password must not be empty.";
        }

        public AccountService(
            UserManager<ApplicationUser> userManager,
            IMapper mapper,
            IConfiguration configuration,
            ILogger<AccountService> logger)
        {
            _userManager = userManager;
            _mapper = mapper;
            _configuration = configuration;
            _logger = logger;
        }

        ArgumentException GetException(string s) => new ArgumentException(JsonConvert.SerializeObject(new[] { s }));
        ArgumentException GetException(IEnumerable<string> s) => 
            new ArgumentException(
                JsonConvert.SerializeObject(
                    s?.ToArray()));

        public async Task Register(RegisterModel model)
        {

            ApplicationUser user;

            try
            {
                user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                    throw GetException(ExceptionMessages.EmailAlreadyExist);
            }
            catch (ArgumentException e)
            {
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occurred during search of user by email.");
                throw GetException(ExceptionMessages.InternalServerError);
            }

            user = new ApplicationUser
            {
                UserName = model.UserName,
                Email = model.Email,
                Balance = StartBalance

            };
            var result = await _userManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
                throw GetException(result.Errors.Select(x => x.Description));
        }

        public async Task<UserModel> GetByName(string name)
        {
            return _mapper.Map<UserModel>(await _userManager.FindByNameAsync(name));
        }

        public async Task<UserModel> GetByEmail(string email)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(email);
                if (user == null)
                {
                    throw GetException(ExceptionMessages.UserNotFound);
                }
                return _mapper.Map<UserModel>(user);
            }
            catch (ArgumentException e)
            {
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occurred during search of user by email.");
                throw GetException(ExceptionMessages.InternalServerError);
            }
        }

        public async Task<UserModel> GetById(string id)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(id);
                if (user == null)
                {
                    throw GetException(ExceptionMessages.UserNotFound);
                }
                return _mapper.Map<UserModel>(user);
            }
            catch (ArgumentException e)
            {
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occurred during search of user by id.");
                throw GetException(ExceptionMessages.InternalServerError);
            }
        }

        public async Task<List<UserModel>> GetOtherUsers(string id)
        {
            try
            {
                var users = await _userManager.Users.Where(q => q.Id!= id).ToListAsync();
                if (users == null)
                {
                    throw GetException(ExceptionMessages.InternalServerError);
                }
                return users.Select(x=>_mapper.Map<UserModel>(x)).ToList();
            }
            catch (ArgumentException e)
            {
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error is get users.");
                throw GetException(ExceptionMessages.InternalServerError);
            }
        }

        public async Task<ApplicationUser> AuthorizeUser(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
            {
                throw GetException(ExceptionMessages.EmailOrPasswordNotNull);
            }
            try
            {
                ApplicationUser user = await _userManager.FindByEmailAsync(email);
                if (user == null)
                    throw GetException(ExceptionMessages.EmailNotExistOrIncorrectPassword);
                var result = await _userManager.CheckPasswordAsync(user, password);
                user = await _userManager.FindByEmailAsync(email);
                if (!result)
                    throw GetException(ExceptionMessages.EmailNotExistOrIncorrectPassword);
                return user;
            }
            catch (ArgumentException e)
            {
                throw;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error occurred during search of user by email or check password.");
                throw GetException(ExceptionMessages.InternalServerError);
            }
        }

        public async Task<IEnumerable<UserModel>> GetByIds(string[] ids)
        {
            var users = await _userManager.Users.Where(x => ids.Contains(x.Id)).ToListAsync();
            return _mapper.Map<IEnumerable<UserModel>>(users);
        }

        public async Task<bool> ChangeBalance(string id, decimal newBalance)
        {
            var user = await _userManager.FindByIdAsync(id);
            
            user.Balance = newBalance;

            // Apply the changes if any to the db
            var result = await _userManager.UpdateAsync(user);            
            return result.Succeeded;
        }
    }
}
