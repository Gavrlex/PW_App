﻿using PW.Data.Entities;
using PW.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PW.BLL.Contracts
{
    public interface IAccountService
    {
        Task Register(RegisterModel model);
        Task<UserModel> GetByName(string name);
        Task<UserModel> GetByEmail(string email);
        Task<UserModel> GetById(string id);
        Task<IEnumerable<UserModel>> GetByIds(string[] ids);
        Task<List<UserModel>> GetOtherUsers(string id);
        Task<ApplicationUser> AuthorizeUser(string email, string password);
        Task<bool> ChangeBalance(string id, decimal newBalance);
    }
}
