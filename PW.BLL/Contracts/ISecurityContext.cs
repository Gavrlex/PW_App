﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PW.BLL.Contracts
{
    public interface ISecurityContext
    {
        Guid CurrentUserId();
    }
}
