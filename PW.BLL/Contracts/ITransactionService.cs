﻿using PW.Data.Models;
using PW.DTO.Paging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PW.BLL.Contracts
{
    public interface ITransactionService
    {
        void Create(OperationParamsModel model);
        Task<PagedList<Data.Entities.Transaction>> GetTransactions(TransactionsPagingParams pagingParams);
    }
}
