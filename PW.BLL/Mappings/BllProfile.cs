﻿using AutoMapper;
using PW.Data.Entities;
using PW.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PW.BLL.Mappings
{
    public class BllProfile : Profile
    {
        public BllProfile()
        {
            CreateMap<Transaction, TransactionModel>()
                .ForMember(x => x.SenderUserID, opt => opt.MapFrom(src => new Guid(src.SenderUserID)))
                .ForMember(x => x.RecipientUserID, opt => opt.MapFrom(src => new Guid(src.RecipientUserID)))
                .ReverseMap();
            CreateMap<ApplicationUser, UserModel>().ReverseMap();
        }
    }
}
