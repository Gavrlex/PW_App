﻿using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PW.Data.Entities;
using PW.DAL.Maps;
using Microsoft.EntityFrameworkCore.Storage;
using PW.DAL.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace PW.DAL
{
    public class PWDbContext : IdentityDbContext<ApplicationUser>, IPWDbContext
    {
        public PWDbContext(DbContextOptions<PWDbContext> options)
            : base(options)
        {
            
        }

        void ICommonbContext.SaveChanges()
        {
            base.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await base.SaveChangesAsync();
        }

        public IDbContextTransaction BeginTransaction()
        {
            return Database.BeginTransaction();
        }

        public DbConnection GetDbConnection()
        {
            return Database.GetDbConnection();
        }

        public int ExecuteSqlCommand(string sqlString)
        {
            return Database.ExecuteSqlCommand(sqlString);
        }

        public Task<int> ExecuteSqlCommandAsync(string sqlString)
        {
            return Database.ExecuteSqlCommandAsync(sqlString);
        }

        public DbSet<Transaction> Transactions { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new TransactionMap());
        }
    }
}
