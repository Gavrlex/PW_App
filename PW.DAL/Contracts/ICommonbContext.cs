﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Data.Common;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Storage;

namespace PW.DAL.Contracts
{
    public interface ICommonbContext : IDisposable 
    {
        void SaveChanges();
        Task SaveChangesAsync();
        IDbContextTransaction BeginTransaction();
        DbConnection GetDbConnection();
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        EntityEntry<TEntity> Entry<TEntity>(TEntity item) where TEntity : class;
        EntityEntry<TEntity> Attach<TEntity>(TEntity item) where TEntity : class;
        int ExecuteSqlCommand(string sqlString);
        Task<int> ExecuteSqlCommandAsync(string sqlString);
    }
}
