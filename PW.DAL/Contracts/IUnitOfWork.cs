﻿using System;
using System.Threading.Tasks;
using PW.Data.Entities;
using Microsoft.EntityFrameworkCore.Storage;
using PW.DAL.Contracts;

namespace PW.DAL.Contracts
{
    public interface IUnitOfWork: IDisposable
    {
        #region repositories
        //IGenericRepository<ApplicationUser> ApplicationUserRepository { get; }
        IGenericRepository<Transaction> TransactionRepository { get; }
        #endregion

        void SaveChanges();
        Task SaveChangesAsync();

        IDbContextTransaction BeginTransaction();
    }

    public interface IUnitOfWork<T> where T:class
    {
        IGenericRepository<T> Service();
        void SaveChanges();
    }
}
