﻿namespace PW.DAL.Contracts
{
    public interface IGenericRepositoryFactory
    {
        IGenericRepository<TEntity> CreateRepository<TEntity>(
            IPWDbContext context
            ) where TEntity : class;
    }
}