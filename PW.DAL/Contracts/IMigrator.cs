﻿namespace PW.DAL.Contracts
{
    public interface IMigrator
    {
        void Migrate();
    }
}
