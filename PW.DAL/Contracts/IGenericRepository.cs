﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PW.DAL.Contracts
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        TEntity Create(TEntity item);
        Task<TEntity> CreateAsync(TEntity item);
        TEntity FindById(string id);
        TEntity FindById(Guid id);
        IQueryable<TEntity> Get();
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate);
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);
        Task<IQueryable<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> predicate, 
            params Expression<Func<TEntity, object>>[] includes);
        Task<IQueryable<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> predicate,
            int? page, int? pageSize,
            params Expression<Func<TEntity, object>>[] includes);
        void Remove(TEntity item);
        void Update(TEntity item);
    }
}