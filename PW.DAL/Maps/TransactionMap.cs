﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PW.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;


namespace PW.DAL.Maps
{
    public class TransactionMap : IEntityTypeConfiguration<Transaction>
    {
        public void Configure(EntityTypeBuilder<Transaction> builder)
        {
            builder.HasKey(a => a.Id);
            builder.HasOne(x => x.RecipientUser);
            builder.HasOne(x => x.SenderUser);
        }
    }
}
