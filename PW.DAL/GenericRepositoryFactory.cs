﻿using PW.DAL.Contracts;

namespace PW.DAL
{
    public class GenericRepositoryFactory : IGenericRepositoryFactory
    {
        public IGenericRepository<TEntity> CreateRepository<TEntity>(
            IPWDbContext context
        ) where TEntity : class
        {
            return new GenericRepository<TEntity>(context);
        }
    }
}
