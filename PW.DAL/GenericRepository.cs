﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using PW.DAL.Contracts;
using Microsoft.EntityFrameworkCore;

namespace PW.DAL
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly IPWDbContext _context;
        protected readonly DbSet<TEntity> DbSet;

        public GenericRepository(IPWDbContext context)
        {
            _context = context;
            DbSet = context.Set<TEntity>();
        }

        public TEntity Create(TEntity item)
        {
            DbSet.Add(item);

            return item;
        }

        public TEntity FindById(Guid id)
        {
            return DbSet.Find(id);
        }

        public IQueryable<TEntity> Get()
        {
            return DbSet.AsNoTracking();
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return DbSet.AsNoTracking().Where(predicate);
        }
        public TEntity FindById(string id)
        {
            return DbSet.Find(id);
        }

        public async Task<TEntity> CreateAsync(TEntity item)
        {
            item = Create(item);
            await SaveChangesAsync();

            return item;
        }

        public void Update(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
            SaveChanges();
        }

        public void Remove(TEntity item)
        {
            if (_context.Entry(item).State == EntityState.Detached)
                _context.Attach(item);

            DbSet.Remove(item);
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes)
        {
            if (includes == null)
                includes = new Expression<Func<TEntity, object>>[0];
            var res = includes
                .Aggregate(DbSet.AsNoTracking(), (current, include) => current.Include(include))
                .Where(predicate);
            return res;
        }

        public async Task<IQueryable<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> predicate, 
            params Expression<Func<TEntity, object>>[] includes)
        {
            if (includes == null)
                includes = new Expression<Func<TEntity, object>>[0];
            var res = await includes
                .Aggregate(DbSet.AsNoTracking(), (current, include) => current.Include(include))
                .Where(predicate)
                .ToListAsync();
            return res.AsQueryable();
        }

        public async Task<IQueryable<TEntity>> GetAsync(
            Expression<Func<TEntity, bool>> predicate,
            int? page, int? pageSize,
            params Expression<Func<TEntity, object>>[] includes)
        {
            if (includes == null)
                includes = new Expression<Func<TEntity, object>>[0];
            var list = includes
                .Aggregate(DbSet.AsNoTracking(), (current, include) => current.Include(include))
                .Where(predicate);

            pageSize = pageSize ?? 10;
            var res = !page.HasValue 
                ? await list.ToListAsync()
                : await list.Skip(page.Value * pageSize.Value).Take(pageSize.Value).ToListAsync();
            return res.AsQueryable();
        }

        

        private void SaveChanges()
        {
            _context.SaveChanges();
        }

        private async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
