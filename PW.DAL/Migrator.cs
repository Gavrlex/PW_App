﻿using PW.DAL.Contracts;
using Microsoft.EntityFrameworkCore;

namespace PW.DAL
{
    public class Migrator : IMigrator
    {
        private readonly PWDbContext _dbAppContext;

        public Migrator(
            PWDbContext dbAppContext)
        {
            _dbAppContext = dbAppContext;
        }

        public void Migrate()
        {
            _dbAppContext?.Database.Migrate();
        }
    }
}
