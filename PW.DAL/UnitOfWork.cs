﻿using System;
using System.Threading.Tasks;
using PW.Data.Entities;
using PW.DAL.Contracts;
using Microsoft.EntityFrameworkCore.Storage;

namespace PW.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private bool _disposed = false;
        private readonly IPWDbContext _dbContext;
        private readonly IGenericRepositoryFactory _repositoryFactory;

        public UnitOfWork(
            IPWDbContext dbContext, 
            IGenericRepositoryFactory repositoryFactory)
        {
            _dbContext = dbContext;
            _repositoryFactory = repositoryFactory;
        }


        private IGenericRepository<Transaction> _transactionRepository;


        public IGenericRepository<Transaction> TransactionRepository
        {
            get
            {
                return _transactionRepository =
                    _transactionRepository
                    ?? _repositoryFactory.CreateRepository<Transaction>(_dbContext);
            }
        }


        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }
        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
        public IDbContextTransaction BeginTransaction()
        {
            return _dbContext.BeginTransaction();
        }


        #region IDisposable
        public virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }
                this._disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
