﻿using PW.Data.Models;
using PW.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using PW.DTO.Paging;

namespace PW.DTO.Mappings
{
    public class DtoProfile : Profile
    {
        public DtoProfile()
        {
            CreateMap<RegisterDTO, RegisterModel>().ReverseMap();
            CreateMap<TransactionDTO, TransactionModel>().ReverseMap();
            CreateMap<UserModel, UserDTO>().ReverseMap();
            CreateMap<OperationParamsModel, OperationParamsDTO>().ReverseMap();
            CreateMap<RepeatOperationParamsModel, RepeatOperationParamsDTO>().ReverseMap();
            CreateMap<TransactionsPagingParamsModel, TransactionsPagingParams>().ReverseMap();
        }
    }
}
