﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PW.DTO.DTO
{
    public class RepeatOperationParamsDTO
    {
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Ид транзакции")]
        public string Id { get; set; }
    }
}
