﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PW.DTO.DTO
{
    public class UserDTO
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        /// <summary>
        /// Баланс счета пользователя
        /// </summary>
        public decimal Balance { get; set; }
    }
}
