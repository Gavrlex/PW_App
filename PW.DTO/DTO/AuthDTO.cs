﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PW.DTO.DTO
{
    public class AuthDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Token { get; set; }
        /// <summary>
        /// Баланс счета пользователя
        /// </summary>
        public decimal Balance { get; set; }
    }
}
