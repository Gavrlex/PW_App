﻿namespace PW.Web.DTO
{
    public class CommonResponse<T> where T: class
    {
        public int Code { get; set; }
        public T Result { get; set; }
    }
}