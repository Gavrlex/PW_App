﻿using PW.DTO.Paging;
using System;
using System.Collections.Generic;
using System.Text;

namespace PW.DTO.DTO
{
    public class TransactionHistoryDTO
    {
        public PagingHeader Paging { get; set; }
        public List<LinkInfo> Links { get; set; }
        public List<TransactionDTO> Items { get; set; }
    }
}
