﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PW.DTO.DTO
{
    public class OperationParamsDTO
    {
        [Required]
        [Display(Name = "Сумма")]
        [Range(0, double.MaxValue)]
        public decimal Amount { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Получатель")]
        public string Recipient { get; set; }
    }
}
