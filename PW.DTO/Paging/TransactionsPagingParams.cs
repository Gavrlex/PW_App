﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PW.DTO.Paging
{
    public class TransactionsPagingParams
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 5;
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public decimal? AmountFrom { get; set; }
        public decimal? AmountTo { get; set; }
        public string CorrespondentName { get; set; }
        public string SortDate { get; set; }
        public string SortName { get; set; }
        public string SortAmount { get; set; }
    }
}
