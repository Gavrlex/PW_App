﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PW.Data.Models
{
    public class OperationParamsModel
    {
        public decimal Amount { get; set; }
        public string Recipient { get; set; }
    }
}
