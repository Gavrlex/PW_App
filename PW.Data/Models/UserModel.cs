﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PW.Data.Models
{
    public class UserModel : BaseModel
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        /// <summary>
        /// Баланс счета пользователя
        /// </summary>
        public decimal Balance { get; set; }
    }
}
