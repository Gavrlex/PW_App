﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PW.Data.Models
{
    public class TransactionsPagingParamsModel
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public decimal? AmountFrom { get; set; }
        public decimal? AmountTo { get; set; }
        public string CorrespondentName { get; set; }
        public bool? SortDescDate { get; set; }
        public bool? SortDescName { get; set; }
        public bool? SortDescAmount { get; set; }
    }
}
