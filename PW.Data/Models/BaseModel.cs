﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PW.Data.Models
{
    public class BaseModel
    {
        public Guid Id { get; set; }
    }
}
