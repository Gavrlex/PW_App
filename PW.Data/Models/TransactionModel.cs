﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PW.Data.Models
{
    public class TransactionModel
    {
        /// <summary>
        /// Id транзакции
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Дата совершения транзакции
        /// </summary>
        public DateTime OperationDateUTC { get; set; }
        /// <summary>
        /// Сумма перевода
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// Отправитель перевода
        /// </summary>
        public Guid SenderUserID { get; set; }
        /// <summary>
        /// Получатель перевода
        /// </summary>
        public Guid RecipientUserID { get; set; }
        /// <summary>
        /// Состояние баланса отправителя
        /// </summary>
        public decimal SenderResultingBalance { get; set; }
        /// <summary>
        /// Состояние баланса получателя
        /// </summary>
        public decimal RecipientResultingBalance { get; set; }

        public UserModel SenderUser { get; set; }
        public UserModel RecipientUser { get; set; }
    }
}
