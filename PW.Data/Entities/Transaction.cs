﻿using PW.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PW.Data.Entities
{
    public class Transaction : BaseEntity
    {
        /// <summary>
        /// Дата совершения транзакции
        /// </summary>
        public DateTime OperationDateUTC { get; set; }
        /// <summary>
        /// Сумма перевода
        /// </summary>
        public decimal Amount { get; set;}
        /// <summary>
        /// Отправитель перевода
        /// </summary>
        public string SenderUserID { get; set; }
        /// <summary>
        /// Получатель перевода
        /// </summary>
        public string RecipientUserID { get; set; }
        /// <summary>
        /// Состояние баланса отправителя
        /// </summary>
        public decimal SenderResultingBalance { get; set; }
        /// <summary>
        /// Состояние баланса получателя
        /// </summary>
        public decimal RecipientResultingBalance { get; set; }

        [ForeignKey("SenderUserID")]
        public ApplicationUser SenderUser { get; set; }
        [ForeignKey("RecipientUserID")]
        public ApplicationUser RecipientUser { get; set; }
    }
}
