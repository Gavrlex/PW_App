﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PW.Data.Entities.Base
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
