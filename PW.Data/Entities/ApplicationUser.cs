﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace PW.Data.Entities
{
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Баланс счета пользователя
        /// </summary>
        public decimal Balance { get; set; }
    }
}
